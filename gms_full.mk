#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# Quick Tap
ifeq ($(TARGET_SUPPORTS_QUICK_TAP), true)
PRODUCT_PACKAGES += \
    quick_tap
endif

# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome \
    Chrome-Stub \
    DevicePolicyPrebuilt \
    Drive \
    GeminiPrebuilt \
    GoogleContacts \
    GoogleTTS \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    Maps \
    MarkupGoogle \
    NgaResources \
    Photos \
    PixelThemesStub \
    PixelThemesStub2022_and_newer \
    PixelWallpapers2023 \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundAmplifierPrebuilt \
    SoundPickerPrebuilt \
    SwitchAccessPrebuilt \
    TrichromeLibrary \
    TrichromeLibrary-Stub \
    Tycho \
    VoiceAccessPrebuilt \
    VZWAPNLib \
    WallpaperEmojiPrebuilt \
    WebViewGoogle \
    WebViewGoogle-Stub \
    arcore \
    talkback

ifeq ($(TARGET_IS_PIXEL), true)
PRODUCT_PACKAGES += \
    GoogleCamera \
    SCONE
endif

ifeq ($(TARGET_PIXEL_STAND_SUPPORTED), true)
PRODUCT_PACKAGES += \
    DreamlinerPrebuilt \
    DreamlinerUpdater
endif

ifeq ($(TARGET_IS_PIXEL_FOLD), true)
PRODUCT_PACKAGES += \
    PixelWallpapers2023Foldable
else ifeq ($(TARGET_IS_PIXEL_8), true)
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2023
else
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2022
endif

# product/priv-app
PRODUCT_PACKAGES += \
    AICorePrebuilt \
    AdaptiveVPNPrebuilt \
    AiWallpapers \
    AmbientStreaming \
    AndroidAutoStubPrebuilt \
    AppDirectedSMSService \
    BetterBugStub \
    CarrierLocation \
    CarrierMetrics \
    CarrierWifi \
    CbrsNetworkMonitor \
    ConfigUpdater \
    ConnMO \
    DCMO \
    DMService \
    DeviceIntelligenceNetworkPrebuilt \
    FilesPrebuilt \
    GCS \
    GoogleDialer \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    HealthIntelligenceStubPrebuilt \
    KidsSupervisionStub \
    MaestroPrebuilt \
    MyVerizonServices \
    OdadPrebuilt \
    OemDmTrigger \
    PartnerSetupPrebuilt \
    Phonesky \
    PixelLiveWallpaperPrebuilt \
    PixelSupportPrebuilt \
    RecorderPrebuilt \
    SafetyHubPrebuilt \
    ScribePrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt \
    Showcase \
    TetheringEntitlement \
    TurboPrebuilt \
    Velvet \
    WallpaperEffect \
    VzwOmaTrigger \
    WeatherPixelPrebuilt \
    WellbeingPrebuilt \
    WfcActivation

# system/app
PRODUCT_PACKAGES += \
    GoogleExtShared \
    GooglePrintRecommendationService

# system/priv-app
PRODUCT_PACKAGES += \
    DocumentsUIGoogle \
    TagGoogle

# system_ext/app
PRODUCT_PACKAGES += \
    EmergencyInfoGoogleNoUi \
    Flipendo

# system_ext/priv-app
PRODUCT_PACKAGES += \
    CarrierSetup \
    ConnectivityThermalPowerManager \
    GoogleFeedback \
    GoogleServicesFramework \
    NexusLauncherRelease \
    PixelDisplayService \
    PixelSetupWizard \
    QuickAccessWallet \
    RilConfigService \
    StorageManagerGoogle \
    WallpaperPickerGoogleRelease \
    grilservice

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    AndroidPlatformServices \
    MlkitBarcodeUIPrebuilt \
    VisionBarcodePrebuilt

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

$(call inherit-product, vendor/gms/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/gms/system/blobs/system_blobs.mk)
$(call inherit-product, vendor/gms/system_ext/blobs/system-ext_blobs.mk)
